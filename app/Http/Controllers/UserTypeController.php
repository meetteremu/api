<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserType;
use Illuminate\Support\Facades\Response;

class UserTypeController extends Controller
{
    public function getAll(){
        $userTypes = UserType::get();

        return Response::json($userTypes,200);
    }

    public function getById($id){
        $userTypes = UserType::find($id);
        return Response::json($userTypes,200);
    }

    public function createUserType(Request $request){
        $userTypes = new UserType;

        $userTypes->name = $request->name;

        $userTypes->save();
        return Response::json($userTypes,200);
    }

    public function updateUserType($id ,Request $request){
        $userTypes = UserType::find($id);
        
        $userTypes->name = $request->name;

        $userTypes->save();
        return Response::json($userTypes,200);
    }

    public function deleteUserType($id){
        UserType::destroy($id);

        return Response::json("Le UserType a bien été supprimée.",200);
    }
}
