<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Response;

class CategoryController extends Controller
{
    public function getAll(){
        $categories = Category::get();

        return Response::json($categories,200);
    }

    public function getById($id){
        $categories = Category::find($id);
        return Response::json($categories,200);
    }

    public function createCategory(Request $request){
        $categories = new Category;

        $categories->name = $request->name;

        $categories->save();
        return Response::json($categories,200);
    }

    public function updateCategory($id ,Request $request){
        $categories = Category::find($id);
        
        $categories->name = $request->name;

        $categories->save();
        return Response::json($categories,200);
    }

    public function deleteCategory($id){
        Category::destroy($id);

        return Response::json("Le Category a bien été supprimée.",200);
    }
}
