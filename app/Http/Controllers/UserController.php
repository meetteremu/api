<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;


class UserController extends Controller
{

    public function login(){ 

        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){ 
           $user = Auth::user();
           $success['token'] =  $user->createToken('Laravel')->accessToken; 
           $success['user'] =  $user; 
            return response()->json($success, 200); 
        } else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function updatePassword() {
        $user = Auth::user();
        $user->update(['password' => Hash::make(request('newPassword'))]);
        return Response::json("Votre mot de passe a bien été modifié.");

    }


    public function getAll(){
        $users = User::get();

        return Response::json($users,200);
    }

    public function getById($id){
        $users = User::find($id);
        return Response::json($users,200);
    }

    public function createUser(Request $request){
        $users = new User;

        $users->username = $request->username;
        $users->password = $request->password;
        $users->user_type_id = $request->user_type_id;

        $users->save();
        return Response::json($users,200);
    }

    public function updateUser($id ,Request $request){
        $users = User::find($id);
        
        $users->name = $request->name;

        $users->save();
        return Response::json($users,200);
    }

    public function deleteUser($id){
        User::destroy($id);

        return Response::json("Le User a bien été supprimée.",200);
    }
}
