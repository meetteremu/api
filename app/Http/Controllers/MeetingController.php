<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meeting;
use Illuminate\Support\Facades\Response;

class MeetingController extends Controller
{
    public function getAll(){
        $meetings = Meeting::with(['user', 'category'])->get();

        return Response::json($meetings,200);
    }

    public function getById($id){
        $meetings = Meeting::find($id);
        return Response::json($meetings,200);
    }

    public function createMeeting(Request $request){
        $meetings = new Meeting;

        $meetings->user_id = $request->userId;
        $meetings->category_id = $request->categoryId;
        $meetings->duration = $request->duration;

        $meetings->save();
        return Response::json($meetings,200);
    }

    public function updateMeeting($id ,Request $request){
        $meetings = Meeting::find($id);
        
        $meetings->user_id = $request->user_id;
        $meetings->category_id = $request->category_id;
        $meetings->duration = $request->duration;

        $meetings->save();
        return Response::json($meetings,200);
    }

    public function deleteMeeting($id){
        Meeting::destroy($id);

        return Response::json("Le Meeting a bien été supprimée.",200);
    }
}
