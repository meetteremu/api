<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            ["admin"],
            ["student"],
        ];

        for($i = 0; $i < sizeof($seeds); $i++):
            UserType::create([
               'name' => $seeds[$i][0],         
            ]);
        endfor;
    }
}
