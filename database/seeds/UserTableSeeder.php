<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            ["Teremu","adminMDP", 1],
            ["Ahuura","secret", 2],
            ["Tea","secret", 2],
            ["Nika","secret", 2],
            ["Maryse","secret", 2],
            ["Barry","secret", 2],
            ["Kaniela","secret", 2],
            ["Matarii","secret", 2],
            ["Manuel","secret", 2],
            ["Tinirau","secret", 2],
            ["Narii","secret", 2],
            ["Kaven","secret", 2],
            ["Revatua","secret", 2],
            ["Fabrice","secret", 2],
        ];

        for($i = 0; $i < sizeof($seeds); $i++):
            User::create([
               'username' => $seeds[$i][0],
               'password' => Hash::make($seeds[$i][1]),
               'user_type_id' => $seeds[$i][2],              
            ]);
        endfor;
    }
}
