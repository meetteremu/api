<?php

use Illuminate\Database\Seeder;
use App\Meeting;

class MeetingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            [2,1],
            [3,2],
            [10,2],
            [5,3],
            [6,1],
            [4,3],
            [8,2],
            [9,1],
            [7,3],
        ];

        for($i = 0; $i < sizeof($seeds); $i++):
            Meeting::create([
               'user_id' => $seeds[$i][0],
               'category_id' => $seeds[$i][1],
               'duration' => '30min',              
            ]);
        endfor;
    }
}
