<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = [
            ["Naming"],
            ["Database"],
            ["Débogage"],
        ];

        for($i = 0; $i < sizeof($seeds); $i++):
            Category::create([
               'name' => $seeds[$i][0],             
            ]);
        endfor;
    }
}
