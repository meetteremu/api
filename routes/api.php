<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', 'UserController@logout');

    //Users
    Route::get("/users","UserController@getAll"); //Récupère tous les users
    Route::get("/users/{id}","UserController@getById"); //Récupère un user par son id
    Route::post("/users/update/{id}","UserController@updateUser"); //Met à jour les infos du user d'un id donné
    Route::post("/users/create","UserController@createUser"); // Création d'un nouvel user
    Route::delete("/users/delete/{id}","UserController@deleteUser"); // Suppression d'un user d'un id donné

    //UserTypes
    Route::get("/user-types","UserTypeController@getAll"); //Récupère tous les user-types
    Route::get("/user-types/{id}","UserTypeController@getById"); //Récupère un userType par son id
    Route::post("/user-types/update/{id}","UserTypeController@updateUserType"); //Met à jour les infos du userType d'un id donné
    Route::post("/user-types/create","UserTypeController@createUserType"); // Création d'un nouvel userType
    Route::delete("/user-types/delete/{id}","UserTypeController@deleteUserType"); // Suppression d'un userType d'un id donné

    //Categories
    Route::get("/categories","CategoryController@getAll"); //Récupère tous les categories
    Route::get("/categories/{id}","CategoryController@getById"); //Récupère un Category par son id
    Route::post("/categories/update/{id}","CategoryController@updateCategory"); //Met à jour les infos du Category d'un id donné
    Route::post("/categories/create","CategoryController@createCategory"); // Création d'un nouvel Category
    Route::delete("/categories/delete/{id}","CategoryController@deleteCategory"); // Suppression d'un Category d'un id donné

    //Meetings
    Route::get("/meetings","MeetingController@getAll"); //Récupère tous les meetings
    Route::get("/meetings/{id}","MeetingController@getById"); //Récupère un Meeting par son id
    Route::post("/meetings/update/{id}","MeetingController@updateMeeting"); //Met à jour les infos du Meeting d'un id donné
    Route::post("/meetings/create","MeetingController@createMeeting"); // Création d'un nouvel Meeting
    Route::delete("/meetings/delete/{id}","MeetingController@deleteMeeting"); // Suppression d'un Meeting d'un id donné


});

